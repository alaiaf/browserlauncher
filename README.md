# BrowserLauncher

Browser Launcher is a tool that allows the testers to speed up the testing process by creating, saving and running a series of combinations of browser/store/environment/country.

[Download BrowserLauncher](https://github.com/alaiaf/BrowserLauncher/blob/eee9b27032538c592bb9ad81e3770720bfa9030e/Package/BrowserLauncher.zip?raw=true)
